#ifndef SYMTABLE_H_H
#define SYMTABLE_H_H
typedef struct Link_node *Link_t;
Link_t LinkTable_new(void);
void LinkTable_free(Link_t oLink);
int LinkTable_getLength(Link_t oLink);
int Link_put(Link_t oLink,
             const char *pcKey,
             const void *pvValue);
void * Link_get(Link_t oLink, const char *pcKey);
int Link_constains(Link_t oLink, const char *pcKey);
void *Link_replace(Link_t oLink,
                      const char *pcKey,
                      const void *pvValue);
void *Link_remove(Link_t oLink, const char *pcKey);
#endif
