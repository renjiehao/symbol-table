/********************************************************
Copyright (C), 2011 - 2014, Multicoreware Inc.
FileName: symtable.c
Author: Version : Renjie:
Description: Establish a Symbol table based on linked list,and control it by functions
Version: v1.0
Function List: 
1. Link_t LinkTable_new(void):establish a new linked list
2. void LinkTable_free(Link_t oLink):free the linked list memory which is pointed by oLink
3. int LinkTable_getLength(Link_t oLink):return length of the linked list 
History:
Renjie 16/07/13 1.0 build this moudle
Renjie 16/07/13 1.0 add Link struct
Renjie 16/07/13 1.0 add LinkTable_new(void)
Renjie 16/07/13 1.0 add LinkTable_free(Link_t oLink)
Renjie 16/07/13 1.0 add LinkTable_getLeLinkTable_new(void)ngth(Link_t oLink)
Renjie 16/07/14 1.0 add function annotate
Renjie 16/07/14 1.0 add refactor LinkTable_new(void)
********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
typedef struct Link_node
{
    char *Key;
    void *value;
    struct Link_node *next;
} *Link_t;
/*************************************************
Function:
LinkTable_new(void)
Description: establish a new linked list,if can't get memory return NULL point
Calls: extern void *malloc(unsigned int num_bytes);
Called By: 
Input:None
Output: a pointer points to a struct  
Return: if can't get memory return NULL point,else return a pointer.
*************************************************/
Link_t LinkTable_new(void)
{
    Link_t start;
    if ((start=(Link_t)malloc(sizeof(struct Link_node))) != NULL )
{
    start->Key = NULL;
    start->value = NULL;
    start->next = NULL;
    return start;
}
    else
{
    printf("malloc is fail\n");
    return NULL;
}
}
/*************************************************
Function:
LinkTable_free(Link_t oLink)
Description: free the linked list memory which is pointed by oLink
Calls: void free(void *ptr)
Called By: 
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
Output: void。
Return: None
Others: if oLink points to NULL,return error
*************************************************/
void LinkTable_free(Link_t  oLink)
{
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return;
}
    Link_t p, q;
    p = oLink;

    if (p->next == NULL) {
      free(p->Key);
      p->Key = NULL;
      p->value = NULL;
      free(p);
      return ;
    }
    while (p != NULL){
    q = p->next;
    free(p->Key);
    p->Key = NULL;
    p->value = NULL;
    free(p);
    p = q;
  }

}
void LinkTable_pfree(Link_t *oLink)
{
    Link_t o;
    o = *oLink;

    if (o == NULL)
{
    printf("The linked list isn't existence\n");
    return ;
}
    else
{

    Link_t p, q;
    p = o->next;

    if (p == NULL)
{
    free(o);
    *oLink = NULL;
    return ;
}
    else
{

    while (p->next != NULL)
{
    free(p->Key);
    q = p->next;
    free(p);
    p = q;
}
    *oLink = NULL;
    free(p);
    free(o);
    return ;
}
}
}
/*************************************************
Function:
LinkTable_getLength(Link_t oLink)
Description: get the lengthof linked list 
Calls: None
Called By: 
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
Output: an int variable represent the length
Return: an int variable
Others: if oLink points to NULL,return error
*************************************************/
int LinkTable_getLength(Link_t oLink)
{

    if (oLink == NULL )
{
    printf("The linked list isn't existence\n");
    return 0;
}
    else
{
    int LinkTable_length = 1;
    Link_t count_point;
    count_point = oLink;
    while(count_point->next != NULL)
{
    count_point = count_point->next;
    LinkTable_length += 1;
}
    return LinkTable_length;
}
}
/*************************************************
Function:
Link_put(Link_t oLink, const char *pcKey, const void *pvValue);
Description: add a node to list 
Calls: None
Called By: 
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
char *pcKey:the symbol you want add to the list
void *pvValue:the symbol value
Output: an int variable 
Return: if add successful,return 1,else return 0
Others: if a same symbol has existed in the list return 0
*************************************************/
int Link_put(Link_t oLink,
                 const char *pcKey,
                 const void *pvValue)
{
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return 0;
}
    else
{
    Link_t erg_node, put_node;
    if (oLink->next == NULL)
{
    if ((put_node = (Link_t)malloc(sizeof(struct Link_node))) != NULL )
{
    //put_node->Key = (char *)pcKey;
    put_node->Key = (char *)malloc(sizeof(pcKey));
    strcpy(put_node->Key, (char *)pcKey);
    put_node->value = (void *)pvValue;
    put_node->next = NULL;
    oLink->next = put_node;
    return 1;
}
    else
{
    return 0;
}
}
    else
{
    erg_node = oLink->next;
    while (erg_node->next != NULL)
{
    if (strcmp(erg_node->Key, pcKey) == 0)
{
      return 0;
}
    erg_node = erg_node->next;
}
    if (strcmp(erg_node->Key, pcKey) == 0)
{
      return 0;
}
    if ((put_node = (Link_t)malloc(sizeof(struct Link_node))) != NULL )
{
    //put_node->Key = (char *)pcKey;
    put_node->Key = (char *)malloc(sizeof(pcKey));
    strcpy(put_node->Key, (char *)pcKey);
    put_node->value = (void *)pvValue;
    put_node->next = NULL;
    erg_node->next = put_node;
    return 1;
}
    else
{
    return 0;
}
}
}
}
/*************************************************
Function:
Link_constains(Link_t oLink, const char *pcKey);
Description: find if a key exists in the linked list
Calls: None
Called By: 
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
char *pcKey:the symbol you want find in the list
Output: an int variable 
Return: if find successful,return 1,else return 0
Others: if the symbol isn't exist in the list return 0
*************************************************/
int Link_constains(Link_t oLink, const char *pcKey)
{
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return 0;
}
    else
{
    Link_t erg_node;
    erg_node = oLink->next;
    while (erg_node != NULL )
{
    if (strcmp(erg_node->Key, pcKey) == 0)
{
    return 1;
}
    erg_node = erg_node->next;
}
    return 0;
}
}

/*************************************************
Function:
Link_get(Link_t oLink, const char *pcKey);
Description: get the value of asymbol in the list
Calls: None
Called By: 
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
char *pcKey:the symbol you want find its value
Return: if find successful,return a pointer point to the value
*************************************************/
void * Link_get(Link_t oLink, const char *pcKey)
{
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return NULL;
}
    else
{
    Link_t erg_node;
    erg_node = oLink->next;
    while (erg_node != NULL )
{
    if (strcmp(erg_node->Key,pcKey) == 0)
{
    return erg_node->value;
}
    erg_node = erg_node->next;
}
    return NULL;
}
}
/*************************************************
Function:
Link_replace(Link_t oLink, const char *pcKey, const void *pvValue);
Description: replace the value of asymbol in the list
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
char *pcKey:the symbol you want to replace its value
void *pvValue:the new value of the symbol you wang to change
Output: an int pointer point to the old value 
Return: if the symbol isn't exist in the list return NULL
*************************************************/
void *Link_replace(Link_t oLink,
                      const char *pcKey,
                      const void *pvValue)
{
    void *oldValue;
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return NULL;
}
    else
{
    Link_t erg_node;
    erg_node = oLink->next;
    while (erg_node != NULL )
{
    if (strcmp(erg_node->Key,pcKey) == 0)
{
    oldValue = erg_node->value;
    erg_node->value = (void *)pvValue;
    return oldValue;
}
    erg_node = erg_node->next;
}
    return NULL;
}
}
/*************************************************
Function:
Link_get(Link_t oLink, const char *pcKey);
Description: remove a symbol node from the linked list
Input:
Link_t oLink:a pointer points to a struct,is the hand pointer in a linked list
char *pcKey:the symbol you want delete its value
Return: if the symbol isn't exist in the list return NULL
*************************************************/
void *Link_remove(Link_t oLink, const char *pcKey)
{
    void *oldValue;
    if (oLink == NULL)
{
    printf("The linked list isn't existence\n");
    return NULL;
}
    else
{
    Link_t erg_node, sta_node;
    erg_node = oLink->next;
    sta_node = oLink;
    while (erg_node != NULL )
{
    if (strcmp(erg_node->Key,pcKey) == 0)
{
    while (sta_node->next != erg_node)
{
    sta_node = sta_node->next;
}
    sta_node->next = erg_node->next;
    oldValue = erg_node->value;
    free(erg_node->Key);
    erg_node->Key = NULL;
    erg_node->value = NULL;
    free(erg_node);
    return oldValue;
}
    erg_node = erg_node->next;
}
    return NULL;
}
}