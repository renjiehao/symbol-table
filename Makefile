all: symtable.o UnitTest.o compile

compile: UnitTest.o symtable.o
	cc -o UnitTest UnitTest.o symtable.o -Wall -std=c99 -pedantic -Wextra

UnitTest.o:UnitTest.c symtable.h
	cc -c UnitTest.c 
symtable.o:
	cc -c symtable.c
test:
	./UnitTest
mem-test:
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes ./UnitTest

clean:
	rm UnitTest UnitTest.o symtable.o