#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "symtable.h"
char *join(char *s1, char *s2)  
{  
    char *ass_str = malloc(strlen(s1)+strlen(s2)+1);//add one for the zero-terminator   
    if (ass_str == NULL) 
{
    exit (1);
}  
    strcpy(ass_str, s1);  
    strcat(ass_str, s2);  
    return ass_str;  
}  

void Try_Case(int expectEquation, const char* msg,int Passed, int Failed)
{
    if (expectEquation == 0)
{
    printf("%s\n", msg);
    printf("-------------------End Unit test---------------------\n");
    printf("Passed : %d\n", Passed);
    printf("Failed : %d\n", Failed);
}
    else 
{
    printf("-------------------End Unit test---------------------\n");
    printf("Passed : %d\n", Passed);
    printf("Failed : 0\n");
}
    
    

}

int main(int argc, char const *argv[])
{
    Link_t head_point = NULL;
    int Passed, Failed, length_count, judge, p_value, iscross,
                chenge_v;
    Passed = 0;
    Failed = 0;
    void *g_value, *c_value;
    void *o, *p;
    char *msg = NULL;
    char *msg_nfree = "Error: LinkTable_free isn't pass normal case:head_point=NULL";
    char *msg_nlong = "Error: LinkTable_new isn't pass normal case:head_point=NULL\n";
    char *msg_nfhand = "Error: LinkTable_getLength isn't pass normal case:length_count!=0\n";
    char *msg_nput = "Error: Link_put isn't pass normal case:input from char []\n";
    char *msg_nputs = "Error: Link_put isn't pass normal case:input from char *\n";
    char *msg_nfin = "Error: Link_get isn't pass normal case:find from head\n";
    char *msg_nfinb = "Error: Link_get isn't pass normal case:find from body\n";
    char *msg_nfinl = "Error: Link_get isn't pass normal case:find from last\n";
    char *msg_nget = "Error: Link_get isn't pass normal case:get from value\n";
    char *msg_nputsa = "Error: Link_put isn't pass normal case:put same symbol\n";
    char *msg_ngetn = "Error: Link_get isn't pass normal case:get symbol isn't exits\n";
    char *msg_nrepn = "Error: Link_replace isn't pass normal case:symbol isn't exits\n";
    char *msg_nrep = "Error: Link_replace isn't pass normal case:replace Failed\n";
    char *msg_nrmn = "Error: Link_remove isn't pass normal case:symbol isn't exits\n";
    char *msg_nrm = "Error: Link_remove isn't pass normal case:remove Failed\n";
    char *msg_nrmh = "Error: Link_remove isn't pass normal case:remove header point Failed\n";
    printf("-------------------Begin Unit test-------------------\n");
    length_count = LinkTable_getLength(head_point);
    if (length_count == 0)
{    
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = "Error: LinkTable_getLength isn't pass normal case:head_point=NULL\n";
}
    LinkTable_free(head_point);
    head_point = NULL;
    judge = LinkTable_getLength(head_point);
    if (judge == 0)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nfree);
}
    head_point = LinkTable_new();
    if (head_point != NULL)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nlong);

}
    length_count = LinkTable_getLength(head_point);
//    printf("%d\n", length_count);
    LinkTable_free(head_point);
    head_point = NULL;
    length_count = LinkTable_getLength(head_point);
//    printf("%d\n", length_count);
    if (length_count == 0)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nfhand);
}
    char nn[] = " ";
    scanf("%s",nn);
    g_value = &p_value;
    p_value = 3;
    head_point = LinkTable_new();
    judge = Link_put(head_point, nn, g_value);
    if (judge == 1)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nput);
}
    p_value = 4;
    judge = Link_put(head_point, "edf", g_value);
    p_value = 5;
    judge = Link_put(head_point, "ccc", g_value);
    if (judge == 1)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nput);
}
    length_count = LinkTable_getLength(head_point);
    if (length_count == 4)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nputs);
}
    judge = Link_constains(head_point, "ee");
    if (judge == 1)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nfin);
}
    judge = Link_constains(head_point, "edf");
    if (judge == 1)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nfinb);
}
    judge = Link_constains(head_point, "ccc");
    if (judge == 1)
{
    Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nfinl);
}
    o = Link_get(head_point, "ee");
//    printf("%p\n", o);
    if (*(int*)o == 5)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nget);
}
    o = Link_get(head_point, "edf");
//    printf("%p\n", o);
    if (*(int*)o == 5)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nget);
}
    o = Link_get(head_point, "ccc");
//    printf("%p\n", o);
    if (*(int*)o == 5)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nget);
}
    o = Link_get(head_point, "xxx");
//    printf("%p\n", o);
    if (o == NULL)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_ngetn);
}
    LinkTable_free(head_point);
    head_point = LinkTable_new();
    judge = Link_put(head_point,"a",g_value);
    judge = LinkTable_getLength(head_point);
    judge = Link_put(head_point,"a",g_value);
    if (judge == 0)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nputsa);
}
    c_value = &chenge_v;
    chenge_v = 9;
    Link_put(head_point,"b",g_value);
    o = Link_replace(head_point, "c", c_value);
    if (o == NULL)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrepn);
}
    p = Link_get(head_point,"b");
    o = Link_replace(head_point, "b", c_value);
    if (o == p)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrep);
}
    p = Link_get(head_point,"b");
        if (o != p)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrep);
}
    o = Link_remove(head_point, "c");
    length_count = LinkTable_getLength(head_point);
    if (o == NULL && length_count == 3)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrmn);
}
    o = Link_remove(head_point, "b");
    length_count = LinkTable_getLength(head_point);
    if (length_count == 2)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrm);
}
    o = Link_remove(head_point, "a");
    length_count = LinkTable_getLength(head_point);
    if (length_count == 1)
{
   Passed += 1;
}
    else
{
    Failed += 1;
    msg = join(msg, msg_nrmh);
}
    LinkTable_free(head_point);
    head_point = NULL;
    if (Failed == 0)
{
    iscross = 1;
}
    else
{
    iscross = 0;
}
    Try_Case(iscross, msg, Passed, Failed);
    free(msg);
    return 0;
}